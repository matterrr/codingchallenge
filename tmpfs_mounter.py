import sys
import argparse
import subprocess
import os
import re

'''
Write a script which will create a tmpfs mount of a certain size. 
Have the name and size as parms passed into the script. 
Check to make sure the mount does not already exist, 
and optionally recreate it (via command line parm). 
Create the directory if needed.
'''


def parse_input():
    parser = argparse.ArgumentParser(description='Create a tmpfs mount of a certain size.')

    parser.add_argument('-s', action='store', dest='size',
                        help='Size of mount. Use format #<unit> e.g. 16G, 32M, etc.')

    parser.add_argument('-n', action='store', dest='name',
                        help='Name for mount. Use only word characters.')

    args = parser.parse_args()

    if not args.size or not args.name:
        print "Specify both name and size. See --help."
        sys.exit(1)

    return args.size, args.name


def check_name(name):
    # Basic regexp - could go more in depth to check valid mount name. Mount name must start with a letter and then
    # use word characters.

    valid_name = re.match('^(/[a-zA-Z]\w+)*$', name)

    if valid_name:
        path_exists = os.path.isdir(name)
    else:
        print "Name is not valid. Exiting."
        sys.exit(1)

    if not path_exists:
        #subprocess.call(['mkdir', name])
        print "Path does not exist. Creating path. (Printing mock command so dir is not actually created):"
        print "mkdir %s" % name
        path_exists = True

    return path_exists and valid_name


def check_size(size):
    # Can make a mount of infinite size. Might want to warn user.
    valid_size = re.match('^[1-9][0-9]+(K|M|G)$', size)
    if not valid_size:
        print "Size format is not valid. See --help."
        sys.exit(1)

    return valid_size


def create_tmpfs(size, name):
    command = 'mount -o size=%s -t tmpfs none %s' % (size, name)

    print str(command)

    return True


def main():
    (size, name) = parse_input()
    name_valid = check_name(name)
    size_valid = check_size(size)

    if name_valid and size_valid:
        create_tmpfs(size, name)
    else:
        print "Size or name invalid. Follow instructions or use --help."


if __name__ == "__main__":
    main()