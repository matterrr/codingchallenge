import sys
import argparse
import subprocess
import re

'''
Write a script which uses multiple calls to ifconfig that reports on each interface which has had traffic,
and how much traffic in and out it has had. Ignore NICs with no traffic. Report in bps, kps, mps based on
both command-line parm and reasonable values. For instance, if the user requests mps, and there was only 200 bps,
then reporting zero would seem silly, so report in bps with a notice.
'''


def parse_input():
    parser = argparse.ArgumentParser(description='Report traffic for interfaces using ifconfig.')

    parser.add_argument('-u', nargs='?', const='bps', action='store', dest='unit',
                        help='Unit for traffic. Possible values are bps, kps, and mps.'
                             'Displays reasonable unit when appropriate or when no unit is specified.')

    unit = parser.parse_args().unit

    if unit not in ('bps', 'kps', 'mps'):
        print 'Unit %s not recognized. Defaulting to bps.' % unit
        unit = 'bps'

    return unit


def ifconfig_interfaces():
    try:
        interfaces = subprocess.check_output(['ifconfig', '-v'])
    except OSError:
        print "Problem calling ifconfig. Check if ifconfig is installed."
        sys.exit(1)

    if interfaces:
        return interfaces
    else:
        print "No interfaces to check. Exiting."
        sys.exit(0)


def formatted_unit(rate, unit):
    unit_converted = unit
    rate_formatted = rate

    # If 1-3 digits and mps or kps specified, use reasonable unit (bps).
    if re.match('^[1-9][0-9]{0,2}$', rate) and unit in ('mps', 'kps'):
        unit_converted = 'bps'

    # If 4-6 digits and mps specified, use reasonable unit (kps)
    elif re.match('^[1-9][0-9]{3,5}$', rate) and unit in 'mps':
        unit_converted = 'kps'

    elif unit in 'mps':
        rate_formatted = str(float(rate) / 1048576.0)

    elif unit in 'kps':
        rate_formatted = str(float(rate) / 1024.0)

    rate_and_unit = "%s %s" % (rate_formatted, unit_converted)

    if unit_converted not in unit:
        rate_and_unit = "%s (used reasonable unit)" % rate_and_unit

    return rate_and_unit


def print_report(interfaces, unit):
    report = ""
    interface = None
    rx_bytes = None
    tx_bytes = None

    for line in interfaces.splitlines():
        if re.search('^[a-zA-Z0-9]', line):
            interface_raw = line.split(' ')[0]
            interface = re.sub(':', '', interface_raw)
        if re.search('RX\spackets', line):
            rx_bytes = line.split()[4]
        elif re.search('TX\spackets', line):
            tx_bytes = line.split()[4]

        if interface and rx_bytes and tx_bytes:
            if_report = "Interface = %s, rx_bytes = %s, tx_bytes = %s" %\
                        (interface, formatted_unit(rx_bytes, unit), formatted_unit(tx_bytes, unit))
            print(if_report)
            report = report + "\n" + if_report
            interface = None
            rx_bytes = None
            tx_bytes = None

    return report


def main():
    traffic_unit = parse_input()
    interfaces = ifconfig_interfaces()
    report = print_report(interfaces, traffic_unit)


if __name__ == "__main__":
    main()
