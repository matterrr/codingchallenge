import unittest
from ifconfig_reporter import print_report


class TestInterfaceReports(unittest.TestCase):
    def test_print_report(self):
        f1 = file('if_unittests/if_1.txt')
        if1_text = f1.read()
        f1.close()

        o1 = file('if_unittests/output_1.txt')
        o1_text = o1.read()
        o1.close()

        report = print_report(if1_text, 'mps')

        self.assertEqual(o1_text, report)


if __name__ == "__main__":
    unittest.main()
